# cmake-fail

Reproduction of a cmake bug.

To replicate the bug:

```
mkdir out
cmake -S . -B out
cmake --build out
```

Then try:

```
touch include/core/this.hpp
cmake --build out
```

which should rebuild as you expect.

```
touch include/extra/this.hpp
cmake --build out
```

doesn't rebuild.
